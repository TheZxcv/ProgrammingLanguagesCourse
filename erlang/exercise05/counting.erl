-module(counting).
-export([start/0, stop/0, dummy1/0, dummy2/0, dummy3/0, top/0]).

update_rec([{Cmd, Counter}|L], Cmd) ->
	[{Cmd, Counter + 1}|L];
update_rec([H|L], Cmd) ->
	[H|update_rec(L, Cmd)].
	
count_server(Records) ->
	receive
		stop -> ok;
		top -> io:format("Record: ~p\n", [Records]),
			count_server(update_rec(Records, top));
		Cmd -> count_server(update_rec(Records, Cmd))
	end.

send_cmd(Cmd) ->
	case whereis(pid_server) of
		undefined -> not_running;
		Pid -> Pid ! Cmd, ok
	end.

start() ->
	case whereis(pid_server) of
		undefined ->
			Rec = [{dummy1, 0}, {dummy2, 0}, {dummy3, 0}, {top, 0}],
			Pid = spawn(fun() -> count_server(Rec) end),
			register(pid_server, Pid),
			ok;
		_ -> already_running
	end.

stop() -> send_cmd(stop).
dummy1() -> send_cmd(dummy1).
dummy2() -> send_cmd(dummy2).
dummy3() -> send_cmd(dummy3).
top() -> send_cmd(top).
