-module(exercise01).
-export([is_palindrome/1, is_an_anagram/2, factors/1, is_proper/1]).

is_palindrome(L) -> is_palindrome_real(filter(L)).

is_palindrome_real([]) -> true;
is_palindrome_real([_]) -> true;
is_palindrome_real([H|L]) ->
	(H == lists:last(L)) and
	is_palindrome_real(lists:droplast(L)).

filter([]) -> [];
filter([H|L]) when H >= $a, H =< $z -> [H|filter(L)];
filter([H|L]) when H >= $A, H =< $Z -> [string:to_lower(H)|filter(L)];
filter([_|L]) -> filter(L).


is_an_anagram(S, L) -> is_an_anagram_real(filter(S), L).

is_an_anagram_real([], _) -> false;
is_an_anagram_real(_, []) -> false;
is_an_anagram_real(S, [H|L]) ->
	T = filter(H),
	((length(S) == length(T)) and ("" == S -- filter(H))) or
	is_an_anagram_real(S, L).


factors(N) when N > 1 -> [X || X <- primes(N), (N rem X) == 0];
factors(_) -> [].

primes(N) when N > 1 ->
	[X || X <- lists:seq(2, N),
	(length([Y || Y <- lists:seq(2, trunc(math:sqrt(X))), ((X rem Y) == 0)]) == 0)];
primes(_) -> [].


is_proper(N) when N > 1 -> lists:sum(proper_divisors(N)) == N;
is_proper(_) -> false.

proper_divisors(N) when N > 1 -> [X || X <- lists:seq(1, N-1), (N rem X) == 0];
proper_divisors(_) -> [].
