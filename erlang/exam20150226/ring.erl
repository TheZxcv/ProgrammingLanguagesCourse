-module(ring).
-export([start/2, send_message/1, send_message/2, stop/0]).

node_loop(Fun, Next, IsLast) ->
	receive
		{req, From, Input, Times} ->
			self() ! {msg, From, Input, Times},
			node_loop(Fun, Next, IsLast);
		{msg, From, Input, 1} when IsLast ->
			From ! {result, Fun(Input)},
			node_loop(Fun, Next, IsLast);
		{msg, From, Input, Repeat} when IsLast ->
			Next ! {msg, From, Fun(Input), Repeat - 1},
			node_loop(Fun, Next, IsLast);
		{msg, From, Input, Repeat} ->
			Next ! {msg, From, Fun(Input), Repeat},
			node_loop(Fun, Next, IsLast);
		die -> Next ! die, io:format("Slave gracefully closed~n");
		Other ->
			io:format("slave: got unexpected message: ~p~n", [Other]),
			node_loop(Fun, Next, IsLast)
	end.


build_ring(Functions) ->
	spawn(fun() -> build_ring(self(), Functions) end).
build_ring(First, [Fun]) ->
	node_loop(Fun, First, true);
build_ring(First, [Fun|Functions]) ->
	Next = spawn_link(fun() -> build_ring(First, Functions) end),
	node_loop(Fun, Next, false).

start(Len, Funs) when Len /= length(Funs) ->
	io:format("Invalid input~n");
start(_, Funs) ->
	register(entrypoint, build_ring(Funs)).

send_cmd(Cmd) ->
	entrypoint ! Cmd.

send_message(Input) -> send_message(Input, 1).
send_message(Input, Times) ->
	send_cmd({req, self(), Input, Times}),
	receive
		{result, Res} -> Res;
		Other -> io:format("send_message: got unexpected message: ~p~n", [Other])
	end.

stop() -> send_cmd(die).
