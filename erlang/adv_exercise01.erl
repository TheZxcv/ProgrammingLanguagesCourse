-module(adv_exercise01).
-export([squared_int/1, intersect/2, symmetric_difference/2]).

squared_int(L) -> [X * X || X <- L, is_integer(X)].

contains(_, []) -> false;
contains(X, [X|_]) -> true;
contains(X, [_|L]) -> contains(X, L).

intersect(L1, L2) -> [X || X <- L1, contains(X, L2)].

symmetric_difference(L1, L2) ->
		Commons=intersect(L1, L2),
		[X || X <- (L1 ++ L2), not contains(X, Commons)].
