-module(combinator).
-export([start/2]).
-import(slave, [init/4]).

start(N, M) ->
	SeqLen = trunc(math:pow(M, N)),
	Powers = [trunc(math:pow(M, X)) || X <- lists:seq(0, N - 1)],
	Head = build_ring(Powers, M, SeqLen),
	SendToHead = fun(Msg) -> Head ! {perm, Msg, []} end,
	lists:foreach(SendToHead, lists:seq(0, SeqLen - 1)),
	wait_for_sequences(0, SeqLen).

build_ring(Periods, Range, Len) ->
	S = self(),
	spawn_link(fun() -> build_ring(S, Periods, Range, Len) end).
build_ring(Head, [Period], Range, Len) ->
	init(Period, Range, Len, Head);
build_ring(Head, [Period|Rest], Range, Len) ->
	Next = spawn_link(fun() -> build_ring(Head, Rest, Range, Len) end),
	init(Period, Range, Len, Next).

format_r([]) -> [];
format_r([Last]) -> io_lib:format("~p", [Last]);
format_r([Head|Tail]) -> [io_lib:format("~p, ", [Head])|format_r(Tail)].
format_list(List) -> lists:flatten(format_r(List)).

wait_for_sequences(Seq, Seq) -> true;
wait_for_sequences(Seq, Last) ->
	receive
		{perm, Seq, Permutation} ->
			io:format("~s~n", [format_list(Permutation)]),
			wait_for_sequences(Seq + 1, Last);
		Other ->
			io:format("combinator: unexpected message: ~p~n", [Other]),
			wait_for_sequences(Seq, Last)
	end.
