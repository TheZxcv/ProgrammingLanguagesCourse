-module(slave).
-export([init/4]).

init(Period, Range, Len, Next) ->
	loop(0, Period, Range, Len, Next).

loop(Seq, _, _, Seq, _) -> true;
loop(Seq, Period, Range, Len, Next) ->
	receive
		{perm, Seq, Permutation} ->
			Next ! {perm, Seq, [((Seq div Period) rem Range) + 1|Permutation]},
			loop(Seq + 1, Period, Range, Len, Next);
		Other ->
			io:format("Unexpected message ~p~n", [Other]),
			loop(Seq, Period, Range, Len, Next)
	end.
