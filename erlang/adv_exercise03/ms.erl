-module(ms).
-export([start/1, close/0, to_slave/2]).

start(N) ->
	register(master,
		spawn(fun() -> master_startup(N) end)),
	true.

spawn_slaves(0) -> [];
spawn_slaves(N) ->
	Pid = spawn_link(fun() -> slave_loop(N) end),
	[{Pid, N}|spawn_slaves(N - 1)].

master_startup(N) ->
	process_flag(trap_exit, true),
	Slaves = spawn_slaves(N),
	master_loop(Slaves).

slave_loop(N) ->
	receive
		die -> exit(killed);
		Any -> io:format("Slave ~p got message ~p~n", [N, Any]), slave_loop(N)
	end.

find_slave_pid(N, Slaves) ->
	{Pid, N} = lists:keyfind(N, 2, Slaves),
	Pid.
find_slave_id(Pid, Slaves) ->
	{Pid, N} = lists:keyfind(Pid, 1, Slaves),
	N.
update_slave({Pid, N}, Slaves) ->
	lists:keyreplace(N, 2, Slaves, {Pid, N}).

master_loop(Slaves) ->
	receive
		{send, Message, N} ->
			Pid = find_slave_pid(N, Slaves),
			io:format("~p~n", [Pid]),
			Pid ! Message,
			master_loop(Slaves);
		{'EXIT', Pid, _} ->
			N = find_slave_id(Pid, Slaves),
			io:format("master restarting dead slave~p~n", [N]),
			NewPid = spawn_link(fun() -> slave_loop(N) end),
			master_loop(update_slave({NewPid, N}, Slaves));
		die ->
			exit(normal)
	end.

to_slave(Message, N) ->
	master ! {send, Message, N}.

close() ->
	master ! die.
