-module(echo).
-export([start/0, stop/0, print/1]).

server_body() ->
	receive
		{msg, Arg} ->
			io:format("~p\n", [Arg]),
			server_body();
		quit -> ok
	end.

start() ->
	case whereis(print_server) of
		undefined ->
			Pid = spawn(fun() -> server_body() end),
			register(print_server, Pid),
			ok;
		_ -> already_running
	end.

stop() ->
	case whereis(print_server) of
		undefined -> not_running;
		Pid -> Pid ! quit, ok
	end.

print(Arg) ->
	case whereis(print_server) of
		undefined -> not_running;
		Pid -> Pid ! {msg, Arg}, ok
	end.
