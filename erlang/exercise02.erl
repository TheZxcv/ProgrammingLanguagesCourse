-module(exercise02).
-export([parse/1, eval/1]).

eval({num, A}) -> A;
eval({tneg, Expr}) -> - eval(Expr);
eval({tadd, Expr1, Expr2}) -> eval(Expr1) + eval(Expr2);
eval({tsub, Expr1, Expr2}) -> eval(Expr1) - eval(Expr2);
eval({tmul, Expr1, Expr2}) -> eval(Expr1) * eval(Expr2);
eval({tdiv, Expr1, Expr2}) -> eval(Expr1) / eval(Expr2);
eval({c_if, Cond, Expr1, Expr2}) ->
	case eval(Cond) of
		0 -> eval(Expr2);
		_ -> eval(Expr1)
	end.


parse(Text) -> parser(lexer(Text)).

parser(Tokens) ->
	{Expr, []}=parse_expr(Tokens),
	Expr.


parse_expr([{num, A},{bin, OP},{num, B}]) ->
		{{OP, {num, A}, {num, B}}, []};
parse_expr([{uni, OP}|L]) ->
	{Expr, Rest}=parse_expr(L),
	{{OP, Expr}, Rest};
parse_expr([{num, A}|L]) ->
		{{num, A}, L};
parse_expr([start|L]) ->
	{Expr, [finish|Rest]}=parse_body(L),
	{Expr, Rest};
parse_expr([c_if|L]) ->
	{Cond, [c_then|Rest]}=parse_expr(L),
	{Expr1, [c_else|Rest1]}=parse_expr(Rest),
	{Expr2, Rest2}=parse_expr(Rest1),
	{{c_if, Cond, Expr1, Expr2}, Rest2}.


parse_body([{num, A},finish|L]) -> {{num, A}, [finish|L]};
parse_body([{uni, OP}|L]) ->
	{Expr, Rest}=parse_expr(L),
	{{OP, Expr}, Rest};
parse_body(L) ->
	{Expr1, Rest}=parse_expr(L),
	[{bin, OP}|Rest1]=Rest,
	{Expr2, Rest2}=parse_expr(Rest1),
	{{OP, Expr1, Expr2}, Rest2}.



lexer([]) -> [];
lexer([$(|L]) -> [start|lexer(L)];
lexer([$)|L]) -> [finish|lexer(L)];
lexer([$+|L]) -> [{bin, tadd}|lexer(L)];
lexer([$-|L]) -> [{bin, tsub}|lexer(L)];
lexer([$*|L]) -> [{bin, tmul}|lexer(L)];
lexer([$/|L]) -> [{bin, tdiv}|lexer(L)];
lexer([$~|L]) -> [{uni, tneg}|lexer(L)];
lexer("if " ++ L) -> [c_if|lexer(L)];
lexer(" then " ++ L) -> [c_then|lexer(L)];
lexer(" else " ++ L) -> [c_else|lexer(L)];
lexer([H|L]) when H >= $0, H =< $9 ->
	{Num, Rest} = lex_num([H|L]),
	[{num, Num}|lexer(Rest)].

lex_num(L) -> string:to_integer(L).
