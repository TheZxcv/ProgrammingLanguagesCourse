-module(distributed).
-export([start/3, ring/2]).

wait_for_msg(Pid, Counter) ->
	io:format("~p: my next is ~p\n", [self(), Pid]),
	io:format("~p: waiting for message\n", [self()]),
	receive
		{msg, Message} ->
			io:format("~p: got ~p\n", [self(), Message]),
			Pid ! {msg, Message},
			io:format("~p: sent ~p to ~p\n", [self(), Message, Pid]),
			wait_for_msg(Pid, Counter+1);
		{quit} ->
			io:format("~p: received quit message\n", [self()]),
			Pid ! {quit},
			io:format("~p: forwarded it, quitting.\n", [self()]),
			io:format("~p: stats: ~p received messages.\n", [self(), Counter])
	end.

send_rep(_, 0, _) -> ok;
send_rep(Pid, Rep, Message) ->
	Pid ! {msg, Message},
	send_rep(Pid, Rep-1, Message).

start(_, 0, _) -> ok;
start(M, N, Message) ->
	Head = make_ring(N),
	send_rep(Head, M, Message),
	Head ! {quit}.

make_ring(N) ->
	spawn(?MODULE, ring, [N, self]).

ring(1, self) ->
	wait_for_msg(self(), 0);
ring(1, Head) ->
	wait_for_msg(Head, 0);
ring(N, self) ->
	Next = spawn(?MODULE, ring, [N-1, self()]),
	wait_for_msg(Next, 0);
ring(N, Head) ->
	Next = spawn(?MODULE, ring, [N-1, Head]),
	wait_for_msg(Next, 0).
