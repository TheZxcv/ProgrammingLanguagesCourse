-module(centralized).
-export([start/3, wait_for_next/0]).

wait_for_next() ->
	receive
		{next, Pid} -> wait_for_msg(Pid, 0)
	end.

wait_for_msg(Pid, Counter) ->
	io:format("~p: my next is ~p\n", [self(), Pid]),
	io:format("~p: waiting for message\n", [self()]),
	receive
		{msg, Message} ->
			io:format("~p: got ~p\n", [self(), Message]),
			Pid ! {msg, Message},
			io:format("~p: sent ~p to ~p\n", [self(), Message, Pid]),
			wait_for_msg(Pid, Counter+1);
		{quit} ->
			io:format("~p: received quit message\n", [self()]),
			Pid ! {quit},
			io:format("~p: forwarded it, quitting.\n", [self()]),
			io:format("~p: stats: ~p received messages.\n", [self(), Counter])
	end.

send_rep(_, 0, _) -> ok;
send_rep(Pid, Rep, Message) ->
	Pid ! {msg, Message},
	send_rep(Pid, Rep-1, Message).

start(_, 0, _) -> ok;
start(M, N, Message) ->
	Head = make_ring(N),
	send_rep(Head, M, Message),
	Head ! {quit},
	ok.

make_ring(N) ->
	Pid = spawn(?MODULE, wait_for_next, []),
	Next = ring(Pid, N-1),
	Pid ! {next, Next},
	Pid.

ring(First, 0) -> First;
ring(First, 1) ->
	Pid = spawn(?MODULE, wait_for_next, []),
	Pid ! {next, First},
	Pid;
ring(First, N) ->
	Pid = spawn(?MODULE, wait_for_next, []),
	Next = ring(First, N-1),
	Pid ! {next, Next},
	Pid.
