-module(sieve).
-export([init/3]).

init(Prime, Head, Next) ->
	loop(Prime, Head, Next).

test_num(N, Prime) -> (N rem Prime) /= 0.

loop(Prime, Head, Next) ->
	receive
		{new, N} ->
			Head ! {pass, N},
			loop(Prime, Head, Next);
		{pass, Prime} ->
			Head ! {res, true},
			loop(Prime, Head, Next);
		{pass, N} when (Prime*Prime > N) ->
			Head ! {res, true},
			loop(Prime, Head, Next);
		{pass, N} -> 
			case test_num(N, Prime) of
				true -> Next ! {pass, N};
				false -> Head ! {res, false}
			end,
			loop(Prime, Head, Next);
		{res, R} ->
			controller ! {res, R},
			loop(Prime, Head, Next);
		Other ->
			io:format("Unexpected message ~p~n", [Other ]),
			loop(Prime, Head, Next)
	end.
