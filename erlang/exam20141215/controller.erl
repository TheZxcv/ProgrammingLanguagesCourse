-module(controller).
-export([start/1]).
-import(sieve, [init/3]).

primes(N) ->
	[X || X <- lists:seq(2, N), 
	      [Y || Y <- lists:seq(2, trunc(math:sqrt(X))), X rem Y == 0] == []].

start(N) ->
	register(controller, self()),
	Primes = primes(N),
	Head = build_ring(Primes),
	loop(Head, lists:last(Primes)).

build_ring(Primes) ->
	spawn_link(fun() -> build_ring(self(), Primes) end).
build_ring(Head, [Mine]) ->
	init(Mine, Head, Head);
build_ring(Head, [Mine|Rest]) ->
	Next = spawn_link(fun() -> build_ring(Head, Rest) end),
	init(Mine, Head, Next).

format(Format, Args) ->
	lists:flatten(io_lib:format(Format, Args)).

loop(Head, Limit) ->
	receive
		{new, N, From} ->
			io:format("You asked for: ~p~n", [N]),
			case (Limit*Limit >= N) of
				true ->
					Head ! {new, N},
					receive
						{res, R} -> From ! {result, format("is ~p prime? ~p", [N, R])}
					end;
				false ->
					From ! {result, format("~p is uncheckable, too big value.", [N])}
			end,
			loop(Head, Limit);
		{quit, From} ->
			io:format("I'm closing ..."),
			From ! {result, "The service is closed!!!"};
		Other ->
			io:format("controller: unexpected message: ~p~n", [Other]),
			loop(Head, Limit)
	end.
