-module(client).
-export([is_prime/1, close/0]).

send_cmd(Cmd) ->
	{controller, server@neptune} ! Cmd,
	receive
		{result, Any} -> Any
	end.

close() -> send_cmd({quit, self()}).
is_prime(N) -> send_cmd({new, N, self()}).
