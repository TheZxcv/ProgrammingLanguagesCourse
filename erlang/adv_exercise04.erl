-module(adv_exercise04).
-export([long_reversed_string/1]).

slave_loop(ID) ->
	receive
		{reverse, From, String} ->
			From ! {reversed, ID, lists:reverse(String)},
			slave_loop(ID);
		die -> io:format("Slave ~p shuts down.~n", [ID])
	end.
reverse_string(Slave, String) ->
	Slave ! {reverse, self(), String}.

split_string_r(String, Offset, _, _) when Offset > length(String) -> [];

split_string_r(String, Offset, ChunkSize, 0) ->
	[string:substr(String, Offset, ChunkSize)|
	 split_string_r(String, Offset + ChunkSize, ChunkSize, 0)];

split_string_r(String, Offset, ChunkSize, Remainder) ->
	[string:substr(String, Offset, ChunkSize + 1)|
	 split_string_r(String, Offset + ChunkSize + 1, ChunkSize, Remainder - 1)].

split_string(_, 0) -> [];
split_string(String, Chunks) ->
	split_string_r(String, 1, length(String) div Chunks, length(String) rem Chunks).

assign_jobs(Slaves, LongString) ->
	Chunks = split_string(LongString, length(Slaves)),
	lists:zipwith(fun(X, Y) -> reverse_string(X, Y) end, Slaves, Chunks).

obtain_results(0) -> [];
obtain_results(Seq) ->
	receive
		{reversed, Seq, String} ->
			[String|obtain_results(Seq-1)]
	end.

combine_results(Slaves) ->
	lists:flatten(obtain_results(length(Slaves))).

master_loop(Slaves) ->
	receive
		{long_reverse, From, LongString} when length(LongString) < 1000 ->
			From ! {long_reversed, lists:reverse(LongString)},
			master_loop(Slaves);
		{long_reverse, From, LongString} ->
			assign_jobs(Slaves, LongString),
			From ! {long_reversed, combine_results(Slaves)},
			master_loop(Slaves);
		die -> io:format("Master shuts down~n"), exit(dies);
		Other -> io:format("Got unexpected ~p~n", [Other])
	end.


make_slaves(0) -> [];
make_slaves(N) ->
	[spawn_link(fun() -> slave_loop(N) end)|make_slaves(N-1)].

master_startup(N) ->
	master_loop(lists:reverse(make_slaves(N))).

launch_master() ->
	Pid = spawn(fun() -> master_startup(10) end),
	register('MasterProcess', Pid),
	Pid.

send_command(Cmd) ->
	case whereis('MasterProcess') of
		undefined -> launch_master() ! Cmd;
		Pid -> Pid ! Cmd
	end.

long_reversed_string(String) ->
	send_command({long_reverse, self(), String}),
	receive
		{long_reversed, Str} -> Str;
		Other -> io:format("Got unexpected ~p~n", [Other])
		after 10000 -> timeout
	end.
