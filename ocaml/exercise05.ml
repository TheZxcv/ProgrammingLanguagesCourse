let sanitize s =
	let rec sanitize s = 
		match s with
		| "" -> []
		| s ->
			match (String.get s 0) with
			| 'a'..'z' as c -> c :: (sanitize (String.sub s 1 ((String.length s)-1)))
			| _ -> sanitize (String.sub s 1 ((String.length s)-1))
	in
	let l = sanitize (String.lowercase s)
	in
	String.init (List.length l) (fun i -> List.nth l i)
;;

let is_palindrome s = 
	let rec is_palindrome s =
		let last = (String.length s) - 1
		in
		match s with
		| "" -> true
		| s when (String.length s) == 1 -> true
		| s ->
			if (String.get s 0) = (String.get s last) then
				is_palindrome (String.sub s
						1
						((String.length s) - 2)
				)
			else false
	in
		is_palindrome (sanitize s)
;;

let primes = [
	('a', 2); ('b', 3); ('c', 5); ('d', 7);
	('e', 11); ('f', 13); ('g', 17); ('h', 19);
	('i', 23); ('j', 29); ('k', 31); ('l', 37);
	('m', 41); ('n', 43); ('o', 47); ('p', 53);
	('q', 59); ('r', 61); ('s', 67); ('t', 71);
	('u', 73); ('v', 79); ('w', 83); ('x', 89);
	('y', 97); ('z', 101);
];;

let find_prime c =
	let rec find_prime c primes =
		match primes with
		| (k, p)::_ when k = c -> p
		| _::tl -> find_prime c tl
		| [] -> 1
	in find_prime c primes
;;

let rec convert_to_num s =
	match s with
	| "" -> 1
	| s ->
		(find_prime (String.get s 0))
		*
		(convert_to_num
			(String.sub s 1 ((String.length s)-1))
		)
;;

let rec anagram str l =
	let is_anagram s1 s2 =
		(convert_to_num s1) = (convert_to_num s2)
	in
	match l with
	| []    -> false
	| h::_ when is_anagram str h -> true
	| _::tl -> anagram str tl
;;

let ( - ) str filter =
	let rec remove str =
		match str with
			| "" -> []
			| s ->
				match (String.get s 0) with
				| c when String.contains filter c -> 
						remove (String.sub s 1 ((String.length s)-1))
				| c -> c :: remove (String.sub s 1 ((String.length s)-1))
	in
	let l = remove str
	in
	String.init (List.length l) (fun i -> List.nth l i)
;;
