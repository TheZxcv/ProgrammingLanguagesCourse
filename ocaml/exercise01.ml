open Printf;;

type elem_data = {name : string; atomic_num : int};;
type element = None | Element of elem_data;;


let cmp_elem_data e1 e2 =
	match e1.atomic_num, e2.atomic_num with
	| (a, b) when a > b -> 1
	| (a, b) when a < b -> -1
	| _,_ -> 0
;;

let cmp_elem e1 e2 =
	match e1, e2 with
	| (Element x, Element y) -> cmp_elem_data x y
	| (Element x, _) -> 1
	| (_, Element y) -> -1
	| (_, _) -> 0
;;

let max_elem e1 e2 =
	match (cmp_elem e1 e2) with
	| -1	-> e2
	| _	-> e1
;;

let rec max_elem_list li =
	match li with
	| h::tl -> max_elem h (max_elem_list tl)
	| [] -> None
;;

let print_elem e =
	match e with
	| Element x	-> Printf.printf "%02d\t%s\n" x.atomic_num x.name
	| _		-> print_string "None\n"
;;

let rec print_elem_list li =
	match li with
	| []	-> ()
	| h::tl ->
		print_elem h;
		print_elem_list tl
;;


let rec merge cmp li1 li2 =
	let merge = merge cmp in
		match li1, li2 with
		| li,[] -> li
		| [],li -> li
		| h1::tl1,h2::tl2	->
			match (cmp h1 h2) with
			| -1 -> h1 :: (merge tl1 li2)
			| 1 -> h2 :: (merge li1 tl2)
			| _ -> h1 :: (merge tl1 tl2)
;;


let rec split li =
	match li with
	| [] -> ([], [])
	| h::[] -> ([h], [])
	| h1::h2::tl -> 
		match split tl with
		(li1, li2) -> (h1::li1, h2::li2)
;;

let rec mergesort merge li =
	let mergesort = mergesort merge in
		match split li with
		| ([], []) -> []
		| ([a], []) -> [a]
		| ([], [b]) -> [b]
		| (li1, li2) -> merge (mergesort li1) (mergesort li2)
;;

let () =
	let alkaline = [
		Element {name="beryllium";atomic_num=4};
		Element {name="magnesium";atomic_num=12};
		Element {name="calcium";atomic_num=20};
		Element {name="strontium";atomic_num=38};
		Element {name="barium";atomic_num=56};
		Element {name="radium";atomic_num=88};
	] in
	let nobles = [
		Element {name="helium";atomic_num=2};
		Element {name="neon";atomic_num=10};
		Element {name="argon";atomic_num=18};
		Element {name="krypton";atomic_num=36};
		Element {name="xenon";atomic_num=54};
		Element {name="radon";atomic_num=86};
	] in
	let merge_elem = (merge cmp_elem) in
	let mergesort_elem = mergesort merge_elem in

	print_elem_list alkaline;
	print_string "\nElement with the highest atomic number:\n";
	print_elem (max_elem_list alkaline);
	print_string "\nSorted merge\n";
	print_elem_list (merge_elem alkaline nobles);
	print_string "\nmergesort\n";
	print_elem_list (mergesort_elem (merge_elem alkaline nobles))
;;
