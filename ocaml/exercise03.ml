type matrix = int list list;;

let rec zeros n m =
	let rec make_row m =
		if m > 0 then
			0 :: make_row (m - 1)
		else
			[]
	in
		if n > 0 then
			(make_row m) :: (zeros (n - 1) m)
		else
			[]
;;

let make_ek_vector k len =
	let rec make_ek_vector i k len =
		if i >= len then
			[]
		else if i = k then
			1 :: (make_ek_vector (i+1) k len)
		else
			0 :: (make_ek_vector (i+1) k len)
	in
		make_ek_vector 0 (k-1) len
;;

let rec identity n =
	let cons a b = List.concat [a;b]
	in
	match n with
	| 0 -> []
	| 1 -> [[1]]
	| n -> (List.map2 cons 
			(identity (n-1))
			(zeros (n-1) 1))
			@ 
		[(make_ek_vector n n)]
;;

let make_1ton_vec n =
	let rec make_1ton_vec i n =
		if i > n then []
		else i :: (make_1ton_vec (i+1) n)
	in
		make_1ton_vec 1 n
;;


let init n =
	let sum a b = a + b
	in
	let rec init i n =
		let f  = sum (n*i)
		in
			if i >= n then []
			else
				(List.map f (make_1ton_vec n)) :: (init (i+1) n)
	in 
		init 0 n
;;

let rec transpose m =
	match m with
	| [] -> []
	| h::[] -> List.map (fun x->[x]) h
	| h::tl -> List.map2 (fun x y -> x::y)
			h
			(transpose tl)
;;

let ( * ) a b =
	let m x y = List.fold_left ( + ) 0 (List.map2 ( * ) x y)
	in
	let rec mul a b =
		match a with
		| [] -> []
		| h1::tl1 ->
				(List.map (m h1) b) :: (mul tl1 b)
	in
	mul a (transpose b)
;;
