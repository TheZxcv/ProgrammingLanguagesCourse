open Printf
open Str
#load "str.cma"

let filter_title s =
	let rec recompose acc l =
		match acc,l with
		| _,[] -> acc
		| "",h::tl -> recompose (acc^h) tl
		| _,h::tl -> recompose (acc^" "^h) tl
	in
		recompose "" (Str.split (Str.regexp "[ ]+") s)
;;

let read_titles f =
	let rec read_titles f =
		try
			let s = input_line f
			in
			filter_title s
				::
			read_titles f
		with
		End_of_file -> []
	in
		read_titles f
;;

let do_numbering l =
	let rec numbering n l =
		match l with
		| [] -> []
		| h::tl -> (n, h)::numbering (n+1) tl
	in
		numbering 1 l
;;

let is_not_minor w =
	match String.lowercase w with
	| "and" | "the" -> false
	| w -> String.length w > 2
;;

let make_tuples l =
	List.flatten (
		List.map
		(fun x ->
			List.map (fun y -> (fst x, y, snd x))
			(List.filter
				is_not_minor
				(Str.split (Str.regexp " ") (snd x))
			)
		)
		l
	)
;;

let print_tuple = function
	(n, k, s) -> Printf.printf "%5d %s\n" n s
;;

let rec print_list l =
	match l with
	| [] -> ()
	| h::tl -> print_tuple h; print_list tl
;;

let main() =
	let f = open_in "titles.txt"
	in
	let l = read_titles f
	in
	close_in f;
	let l = List.sort
		(fun x y -> match x,y with
			(_,s,_),(_,t,_) -> String.compare s t)
		(make_tuples (do_numbering l))
	in
	print_list l
;;
main()
