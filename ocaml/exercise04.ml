let read_word f =
	let rec read_word() =
		match Char.lowercase(input_char f) with
		| 'a'..'z' | '0'..'9' as a -> a::(read_word())
		| _ -> []
	in
	try
		let l = read_word()
		in
		Some (String.init
			(List.length l)
			(fun i -> List.nth l i))
	with
		End_of_file -> None
;;

module Dict = Map.Make(String);;

let read_all_words f =
	let get_value w d =
		try Dict.find w d
		with Not_found -> 0
	in
	let rec read_all_words d =
		match read_word f with
		| None -> d
		| Some w -> read_all_words
				(Dict.add w ((get_value w d)+1) d)
	in
	read_all_words Dict.empty
;;

let build_dict filename =
	let f = open_in filename
	in
	let d = read_all_words f
	in
	close_in f; d
;;
