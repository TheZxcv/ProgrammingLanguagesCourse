module Graph :
	sig
		type relationship = Friend | CoWorker
		type graph
		type node
		type edge

		exception EmptyGraph

		val empty : graph
		val add_node : string -> graph -> graph
		val add_edge : string -> string -> relationship -> graph -> graph
		val find_node : string -> graph -> node
		val node_list : graph -> node list
		val print_graph : graph -> unit
	end =
	struct
		type relationship = Friend | CoWorker
		type node = Node of string
		type edge = node * node * relationship
		type graph = Empty | Graph of node list * edge list

		exception EmptyGraph

		let empty = Empty
		let add_node v = function
			| Empty -> Graph ([(Node v)],[])
			| Graph (l,e) -> Graph ((Node v::l),e)

		let find_node v g =
			let rec find_node l =
				match l with
				| [] -> raise Not_found
				| (Node a) as h::tl ->
						if a = v then h
						else find_node tl
			in
				match g with
				| Graph (l,_) -> find_node l
				| Empty -> raise EmptyGraph

		let add_edge v1 v2 rel g =
			let add_edge v1 v2 e =
				match (find_node v1 g),(find_node v2 g) with
				| x,y -> (x,y,rel)::e
			in
			match g with
			| Empty -> raise EmptyGraph
			| Graph (l,e) -> 
				try Graph (l, (add_edge v1 v2 e))
				with Not_found -> g

		let node_list = function
			| Empty -> []
			| Graph (l,_) -> l

		let print_graph g =
			let rec print_nodes l =
				match l with
				| Node s::tl -> print_endline s; print_nodes tl
				| [] -> ()
			in
			match g with
			| Empty -> ()
			| Graph (l,_) -> print_nodes l
	end
;;
