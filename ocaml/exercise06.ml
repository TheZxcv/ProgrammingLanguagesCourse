
let fact n =
	let rec fact acc = function
		0 -> acc | n -> fact (n*acc) (n - 1)
	in
		float_of_int (fact 1 n)
let pow x n =
	x ** (float_of_int n)

let my_sin x n =
	let ith i =
		let grade = 2*i + 1 in
		(if i mod 2 = 0 then 1.0 else -1.0)
			*.
		((pow x grade) /. (fact grade))
	in
	let rec sin acc = function
		| step when step = n -> acc 
		| step -> sin (acc +. (ith step)) (step + 1)
	in
		sin 0.0 0

let my_cos x n = 
	let ith i =
		let grade = 2*i in
		(if i mod 2 = 0 then 1.0 else -1.0)
			*.
		((pow x grade) /. (fact grade))
	in
	let rec cos acc = function
		| step when step = n -> acc 
		| step -> cos (acc +. (ith step)) (step + 1)
	in
		cos 0.0 0

let err f g x = (f x) -. (g x)
let sin_err = err sin (fun x -> my_sin x 10)
let cos_err = err cos (fun x -> my_cos x 10)

let main() =
	print_float (sin_err 1.);
	print_endline "";
	print_float (cos_err 1.);
	print_endline ""
;;
main()
