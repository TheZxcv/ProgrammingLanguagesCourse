open Printf;;

type scale = Kelvin | Celsius | Fahrenheit | Rankine | Delisle | Newton | Reaumur | Romer;;
let all_scales = [Kelvin;Celsius;Fahrenheit;Rankine;Delisle;Newton;Reaumur;Romer];;
type temperature = float * scale;;
exception WrongUnit;;
exception NoSuchUnit;;

(*[(Kelvin, "K"),(Celsius, "C");(Fahrenheit, "F");(Rankine, "R");(Delisle, "De");(Newton, "N");(Reaumur, "Re\'");(Romer, "Ro")]*)
let print_temp = function
	temp,scale ->
		Printf.printf "%0.02f " temp;
		match scale with
			| Kelvin	-> print_endline "K"
			| Celsius	-> print_endline "C"
			| Fahrenheit	-> print_endline "F"
			| Rankine	-> print_endline "R"
			| Delisle	-> print_endline "De"
			| Newton	-> print_endline "N"
			| Reaumur	-> print_endline "Re\'"
			| Romer		-> print_endline "Ro"
;;

let toKelvinFromCelsius = fun temp -> temp +. 273.15;;
let toKelvinFromFahrenheit = fun temp -> (temp +. 459.67) *. (5. /. 9.);;
let toKelvinFromRankine = fun temp -> temp *. (5. /. 9.);;
let toKelvinFromDelisle = fun temp -> 373.15 -. temp *. (2. /. 3.);;
let toKelvinFromNewton = fun temp -> temp *. 33. /. 100. +. 273.15;;
let toKelvinFromReaumur = fun temp -> temp *. 4. /. 5. +. 273.15;;
let toKelvinFromRomer = fun temp -> (temp -. 7.5) *. 40. /. 21. +. 273.15;;

let toKelvinFuns = [
	(Celsius, toKelvinFromCelsius);
	(Fahrenheit, toKelvinFromFahrenheit);
	(Rankine, toKelvinFromRankine);
	(Delisle, toKelvinFromDelisle);
	(Newton, toKelvinFromNewton);
	(Reaumur, toKelvinFromReaumur);
	(Romer, toKelvinFromRomer)
];;


let fromKelvinToCelsius = fun temp -> temp -. 273.15
let fromKelvinToFahrenheit = fun temp -> temp *. (9. /. 5.) -. 459.67
let fromKelvinToRankine = fun temp -> temp *. (9. /. 5.)
let fromKelvinToDelisle = fun temp -> (373.15 -. temp) *. (3. /. 2.)
let fromKelvinToNewton = fun temp -> (temp -. 273.15) *. (33. /. 100.)
let fromKelvinToReaumur = fun temp -> (temp -. 273.15) *. (4. /. 5.)
let fromKelvinToRomer = fun temp -> (temp -. 273.15) *. 21. /. 40. +. 7.5

let fromKelvinFuns = [
	(Celsius, fromKelvinToCelsius);
	(Fahrenheit, fromKelvinToFahrenheit);
	(Rankine, fromKelvinToRankine);
	(Delisle, fromKelvinToDelisle);
	(Newton, fromKelvinToNewton);
	(Reaumur, fromKelvinToReaumur);
	(Romer, fromKelvinToRomer)
];;

let convertToKelvin temp =
	let rec convertToKelvin li temp =
		match temp,li with
		| (_, Kelvin),_ -> temp
		| (num, scale1),(scale2, func)::_ 
			when scale1 = scale2 -> (func num, Kelvin)
		| temp,_::tl -> convertToKelvin tl temp
		| _,[] -> raise NoSuchUnit
	in
		convertToKelvin toKelvinFuns temp
;;

let convertFromKelvin temp toscale =
	let rec convertFromKelvin li temp toscale =
		match temp,li with
		| (_, scale),_ when scale <> Kelvin -> raise WrongUnit
		| temp,_ when toscale = Kelvin -> temp
		| (num, Kelvin),(scale, func)::_ when scale = toscale -> (func num, toscale)
		| temp,_::tl -> convertFromKelvin tl temp toscale
		| _,[] -> raise NoSuchUnit
	in
		convertFromKelvin fromKelvinFuns temp toscale
;;

let convert temp toscale =
	convertFromKelvin (convertToKelvin temp) toscale
;;

let conversionTableFromScale temp = 
	let convertTo = convert temp
	in
	let rec conversionTableFromScale li temp =
		match li with
		| [] -> ()
		| h::tl ->
			print_temp (convertTo h);
			conversionTableFromScale tl temp
	in
		conversionTableFromScale all_scales temp
;;

let conversionTable num =
	let rec conversionTable li num =
		match li with
		| [] -> ()
		| h::tl ->
			conversionTableFromScale (num,h);
			print_newline ();
			conversionTable tl num
	in
		conversionTable all_scales num
;;

let main() =
	print_newline ();
	conversionTable 10.0;
	conversionTableFromScale (10.0, Celsius)
;;
main();;
