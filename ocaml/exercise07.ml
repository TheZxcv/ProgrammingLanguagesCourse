module PolishCalculator =
	struct
		type unary = Plus | Minus
		type binary = Add | Sub | Mul | Div | Pow
		type expr = Nothing | Constant of int | Unary of expr * unary | Binary of expr * expr * binary

		exception NoSuchOperator

		let sym_unary = [(Plus, "+", (fun x -> x)); (Minus, "-", (fun x -> -x))]
		let sym_binary = [
			(Add, "+", (+));
			(Sub, "-", (-));
			(Mul, "*", ( * ));
			(Div, "/", (/));
			(Pow, "**", (fun x y -> int_of_float ((float_of_int x) ** (float_of_int y))))
		]

		let rec find_sym (op : 'a) (li : ('a * string * _) list) =
			match li with
			| [] -> raise NoSuchOperator
			| (o, c, _)::_ when o = op -> c
			| _::tl -> find_sym op tl

		let rec get_op (op : 'a) (li : ('a * _ * 'b) list) =
			match li with
			| [] -> raise NoSuchOperator
			| (o, _, f)::_ when o = op -> f
			| _::tl -> get_op op tl

		let find_sym_unary u =
			find_sym u sym_unary
		let get_unary_op u =
			get_op u sym_unary
		let find_sym_binary b =
			find_sym b sym_binary
		let get_binary_op b =
			get_op b sym_binary

		let rec eval e =
			match e with
			| Nothing -> 0
			| Constant n -> n
			| Unary (e,u) -> (get_unary_op u) (eval e)
			| Binary (e1,e2,b) -> (get_binary_op b) (eval e1) (eval e2)

		let rec tostring e =
			match e with
			| Nothing -> ""
			| Constant n -> string_of_int n
			| Unary (e,u) -> (tostring e) ^ " " ^ (find_sym_unary u)
			| Binary (e1,e2,b) -> (tostring e1) ^ " " ^ (tostring e2) ^ " " ^ (find_sym_binary b)
	end
;;

module PC = PolishCalculator

let main() =
	let e = PC.Binary (PC.Binary (PC.Constant 3, PC.Constant 4, PC.Add), PC.Constant 5, PC.Mul)
	in
		print_endline (PC.tostring e);
		print_endline (string_of_int (PC.eval e));
;;
main()
