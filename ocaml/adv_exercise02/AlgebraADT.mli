module type AlgebraADT =
	sig
		type a
		val set : a list
		val add : a -> a -> a
		val mul : a -> a -> a
		val id : a
	end;;
