open AlgebraADT

module Ring (A : AlgebraADT) =
	struct
		let check_commutativity() =
			let rec check_commu l1 =
				match l1 with
				| [] -> true
				| h::tl -> List.fold_left ( && ) true
						(List.map
							(fun x -> (A.add x h) = (A.add h x))
							A.set
						) && check_commu tl
			in
				check_commu A.set

		let check_distributivity() =
			let rec check_distr l1 =
				match l1 with
				| [] -> true
				| h::tl -> List.fold_left ( && ) true
						(List.map2
							(fun x y ->
								(A.mul h (A.add x y)) =	(A.add (A.mul h x) (A.mul h y))
							)
							A.set A.set
						) && check_distr tl
			in
				check_distr A.set
	end;;
