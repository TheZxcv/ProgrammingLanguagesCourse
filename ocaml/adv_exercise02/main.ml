open Printf

open BooleanAlgebra
open ModuleAlgebra
open StringAlgebra

open Monoid
open Group
open Ring

module M = Monoid(BooleanAlgebra)

let print_bool (b : bool) =
	Printf.printf "%B\n" b


let main() =
	print_bool (M.check_identity())
;;
main()
