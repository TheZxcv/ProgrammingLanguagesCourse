module BooleanAlgebra =
	struct
		type a = bool
		let set = [true; false]
		let add a b = a || b
		let mul a b = a && b
		let id = false
	end;;
