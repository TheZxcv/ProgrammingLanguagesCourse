open AlgebraADT

module Group (A : AlgebraADT) =
	struct
		let check_closure() =
			let rec check_closure l =
				match l with
				| [] -> true
				| h::tl -> List.fold_left ( && ) true
						(List.map
							(fun x -> List.exists (fun e -> e = (A.add x h)) A.set)
							A.set
						) && check_closure tl
			in
				check_closure A.set

		let check_invertibility() =
			let rec check_invert l =
			match l with
			| [] -> true
			| h::tl -> List.exists (fun x -> (A.add x h) = A.id) A.set
					&& check_invert tl
			in
				check_invert A.set
	end;;
