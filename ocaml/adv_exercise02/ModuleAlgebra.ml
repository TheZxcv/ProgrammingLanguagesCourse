module ModuleAlgebra =
	struct
		type a = int
		let set = [0;1;2;3;4;5;6;7]
		let add a b = (a + b) mod (List.length set)
		let mul a b = (a * b) mod (List.length set)
		let id = 0
	end;;
