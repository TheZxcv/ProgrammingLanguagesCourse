open AlgebraADT

module Monoid (A : AlgebraADT) =
	struct
		let check_identity() =
			let rec check_identity l =
				match l with
				| [] -> true
				| a::tl -> (A.add a A.id) = a && check_identity tl
			in
				check_identity A.set

		let check_associativity() =
			let rec check_assoc l1 =
				match l1 with
				| [] -> true
				| h::tl -> List.fold_left ( && ) true
						(List.map2
							(fun x y ->
								(A.add h (A.add x y)) =	(A.add (A.add h x) y)
							)
							A.set A.set
						) && check_assoc tl
			in
				check_assoc A.set
	end;;
