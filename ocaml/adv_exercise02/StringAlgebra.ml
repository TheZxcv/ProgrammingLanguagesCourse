module StringAlgebra =
	struct
		type a = string
		let set = ["a"; "aa"; "aaa"; "aaaa"]
		let add a b = if String.length a < String.length b then b else a
		let mul a b = a ^ b
		let id = ""
	end;;
