open NaturalI

module N : NaturalI =
	struct
		type natural = Zero | Succ of natural
		exception NegativeNumber
		exception DivisionByZero

		let rec eval nat =
			match nat with
			| Succ a -> 1 + (eval a)
			| Zero -> 0

		let rec convert num =
			if num < 0 then raise NegativeNumber;
			if num = 0
			then Zero
			else Succ (convert (num - 1))


		let rec ( + ) num1 num2 =
			match num1,num2 with
			| Zero,_ -> num2
			| _,Zero -> num1
			| num,Succ(a) -> Succ(num + a)

		let rec ( - ) num1 num2 =
			match num1,num2 with
			| Zero,Zero 	-> Zero
			| Zero,Succ _ 	-> raise NegativeNumber
			| num,Zero 	-> num
			| Succ a,Succ b	-> a - b

		let rec ( * ) num1 num2 = 
			match num1,num2 with
			| _,Zero	-> Zero
			| Zero,_	-> Zero
			| num,Succ Zero	-> num
			| a,Succ b	-> a + (a * b)

		let rec ( / ) num1 num2 = 
			match num1,num2 with
			| _,Zero	-> raise DivisionByZero
			| Zero,_	-> Zero
			| num,Succ Zero	-> num
			| a, b	-> try
					Succ(Zero) + ((a - b) / b)
				with NegativeNumber -> Zero

	end;;
