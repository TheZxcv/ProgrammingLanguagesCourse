module KFloatVChar =
	struct
		type key = float
		type value = char

		let _NullValue = ' '
		let isNull x = (x == _NullValue)
	end;;
