module type KeyValue =
	sig
		type key
		type value

		val _NullValue: value
		val isNull: value -> bool
	end;;
