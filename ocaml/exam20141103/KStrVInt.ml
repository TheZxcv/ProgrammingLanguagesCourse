module KStrVInt =
	struct
		type key = string
		type value = int

		let _NullValue = -1
		let isNull x = (x == _NullValue)
	end;;
