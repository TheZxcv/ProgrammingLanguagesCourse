open KeyValue
open KFloatVChar
open KStrVInt

module Dictionary (KV : KeyValue) =
	struct
		exception KeyNotFound

		let empty = fun (_ : KV.key) -> KV._NullValue
		let add d k v = fun k' -> if k' = k then v else d k'

		let find d k =
			if KV.isNull (d k) then
				raise KeyNotFound
			else
				d k
			
		let remove d k =
			if KV.isNull (d k) then
				raise KeyNotFound
			else
				(fun k' ->
					if k' = k then KV._NullValue
					else d k')
	end;;

module FCDict = Dictionary(KFloatVChar)
module SIDict = Dictionary(KStrVInt)
