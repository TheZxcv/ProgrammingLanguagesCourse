let is_prime n =
	let rec is_prime step n =
		match n,step with
		| 1,_ -> false
		| _,1 -> true
		| 2,_ -> true
		| n,step when n mod step != 0 -> is_prime (step - 1) n
		| _,_ -> false
	in
		is_prime
			(int_of_float (sqrt (float_of_int n)))
			n

let primes n =
	let rec primes i n =
		match i with
		| _ when i >= n -> []
		| 2 -> 2::primes 3 n
		| _ when is_prime i -> i::primes (i+2) n
		| _  -> primes (i+2) n
	in
		primes 2 n

exception LessOrEqualToTwo
let goldbach n =
	let rec test p =
		match p with
		| [] -> raise Not_found
		| h1::tl ->
			match List.filter (fun x -> h1 + x = n) p with
			| h2::_ -> (h1, h2)
			| _ -> test tl
	in
		if n > 2 then test (primes n)
		else raise LessOrEqualToTwo


let goldbach_list (n, m) =
	let rec goldbach_list i =
		match i with
		| _ when i mod 2 = 1 -> goldbach_list (i+1)
		| _ when i > m -> []
		| _ -> goldbach i :: goldbach_list (i+2)
	in
		goldbach_list n
