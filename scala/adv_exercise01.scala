import scala.collection.mutable.Stack
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.io.Source

trait Token
case class OpenPar() extends Token
case class ClosePar() extends Token
case class Operand(kind: OperandKind) extends Token
case class Operator(kind: OperatorKind) extends Token

class OperandKind
case class Number(n: Double) extends OperandKind
case class Var(v: String) extends OperandKind

class OperatorKind(val priority: Int)
case class Equal() extends OperatorKind(0)
case class Plus() extends OperatorKind(1)
case class Minus() extends OperatorKind(1)
case class Times() extends OperatorKind(2)
case class Div() extends OperatorKind(2)
case class Sqrt() extends OperatorKind(3)
case class Sine() extends OperatorKind(3)
case class Cosine() extends OperatorKind(3)
case class Tan() extends OperatorKind(3)
case class Log() extends OperatorKind(3)
case class Power() extends OperatorKind(4)

object adv_exercise01 {
	val vars: Map[String, Double] = Map()

	def groupByFun[T](list: List[T], fun: (T, T) => Boolean) : List[List[T]] =
		list match {
			case Nil => Nil
			case h::tl =>
				val segment = h :: tl.takeWhile {fun(h, _)}
				segment :: groupByFun(list.drop(segment.length), fun)
		}

	def tokenize(line: String) : List[Token] = {
		groupByFun(line.toList, (
			(a: Char, b: Char) =>
				(a.isLetter && b.isLetter) ||
				(a.isDigit && b.isDigit) ||
				(a == ' ' && b == ' ')
			)
		)
		.map(_.mkString)
		.filter { !_.contains(' ') }
			.map {
				_ match {
					case "(" => OpenPar()
					case ")" => ClosePar()
					case "=" => Operator(Equal())
					case "+" => Operator(Plus())
					case "-" => Operator(Minus())
					case "*" => Operator(Times())
					case "/" => Operator(Div())
					case "^" => Operator(Power())
					case "sqrt" => Operator(Sqrt())
					case "sin" => Operator(Sine())
					case "cos" => Operator(Cosine())
					case "tan" => Operator(Tan())
					case "log" => Operator(Log())
					case v: String if !v.charAt(0).isDigit => Operand(Var(v))
					case x: String =>
							try
								Operand(Number(x.toInt))
							catch {
								case e: Exception => throw new Exception("Unrecognized symbol \"" + x + "\"")
							}
				}
			}.toList
	}

	def toPostFix(tokens: List[Token]) = {
		val stack = new Stack[Token]
		val output = new ListBuffer[Token]
		tokens.foreach {
			_ match {
				case p: OpenPar => stack.push(p)
				case p: ClosePar =>
						while(!stack.isEmpty && !stack.top.isInstanceOf[OpenPar])
							output.append(stack.pop)
						assert(!stack.isEmpty)
						stack.pop
				case Operator(kind) =>
						var over = false
						while(!stack.isEmpty && !over) {
							stack.top match {
								case n: Operand =>
									output.append(stack.pop)
								case Operator(k) =>
									if (k.priority > kind.priority)
										output.append(stack.pop)
									else
										over = true
								case _ => over = true
							}
						}
						stack.push(Operator(kind))
				case o: Operand => output.append(o)
			}
		};
		while(!stack.isEmpty)
			output.append(stack.pop)
		output.toList
	}

	def getOperandValue(operand: OperandKind): Double =
		operand match {
			case Number(n) => n
			case Var(v) => vars.get(v).get
		}
	def setOperandValue(operand: OperandKind, value: Double): Double =
		operand match {
			case Number(n) => throw new Exception("impossible")
			case Var(v) => vars.put(v, value); value
		}

	def eval(tokens: List[Token]) = {
		val stack = new Stack[OperandKind]
		tokens.foreach {
			_ match {
				case Operand(n) => stack.push(n)
				case Operator(kind) =>
					kind match {
						case _: Equal =>
									val b = getOperandValue(stack.pop)
									stack.push(Number(setOperandValue(stack.pop, b)))
						case _: Plus =>
									val b = getOperandValue(stack.pop)
									val a = getOperandValue(stack.pop)
									stack.push(Number(a + b))
						case _: Minus =>
									val b = getOperandValue(stack.pop)
									val a = getOperandValue(stack.pop)
									stack.push(Number(a - b))
						case _: Times =>
									val b = getOperandValue(stack.pop)
									val a = getOperandValue(stack.pop)
									stack.push(Number(a * b))
						case _: Div =>
									val b = getOperandValue(stack.pop)
									val a = getOperandValue(stack.pop)
									stack.push(Number(a / b))
						case _: Sqrt =>
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.sqrt(a)))
						case _: Sine =>
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.sin(a)))
						case _: Cosine =>
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.cos(a)))
						case _: Tan =>
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.tan(a)))
						case _: Log =>
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.log(a)))
						case _: Power =>
									val n = getOperandValue(stack.pop)
									val a = getOperandValue(stack.pop)
									stack.push(Number(math.pow(a, n)))
					}
			}

		};
		getOperandValue(stack.pop)
	}

	def compute(line: String) = {
		eval(toPostFix(tokenize(line)))
	}

	def main(args: Array[String]) = {
		Source.fromFile("script").getLines.foreach {expr => println(expr + " = " + compute(expr))}
	}
}
