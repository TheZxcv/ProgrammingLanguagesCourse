import scala.collection.mutable.Buffer
import scala.collection.mutable.Stack

object Exercise08 {
	def main(args: Array[String]) = {
		val e = new Editor("ciao prova ciao") with UndoRedo with Debug
		e.l(4)
		println(e)
		e.iw("ciao")
		println(e)
		e.u
		println(e)
		e.ctrlr
		println(e)
	}
}

trait UndoRedo extends Editor {
	val history: Stack[Tuple2[Int, Buffer[Char]]] = new Stack
	val changes: Stack[Tuple2[Int, Buffer[Char]]] = new Stack

	def updateHistory = {
		history.push((cur_pos, buffer.clone()))
		changes.clear()
	}

	override def x = {updateHistory; super.x}
	override def dw = {updateHistory; super.dw}
	override def i(c: Char) = {updateHistory; super.i(c)}
	override def iw(word: String) = {updateHistory; super.iw(word)}
	override def l(n: Int = 1) = {updateHistory; super.l(n)}
	override def h(n: Int = 1) = {updateHistory; super.h(n)}

	def u =
		if(!history.isEmpty) {
			changes.push((cur_pos, buffer))
			val (c, b) = history.pop()
			cur_pos = c
			buffer = b
		}

	def ctrlr =
		if(!changes.isEmpty) {
			history.push((cur_pos, buffer))
			val (c, b) = changes.pop()
			cur_pos = c
			buffer = b
		}
}

trait Debug extends Editor {
	var lastOp = ""

	override def x = {lastOp = "x"; super.x}
	override def dw = {lastOp = "dw"; super.dw}
	override def i(c: Char) = {lastOp = "i"; super.i(c)}
	override def iw(word: String) = {lastOp = "iw"; super.iw(word)}
	override def l(n: Int = 1) = {lastOp = "l"; super.l(n)}
	override def h(n: Int = 1) = {lastOp = "h"; super.h(n)}

	override def toString = {
		"last operation: " + lastOp + ", cursor=" + (cur_pos+1) + "\n" +
		super.toString + "\n" + 
		(" " * cur_pos) + "^"
	}
}

class Editor(line: String) {
	var buffer: Buffer[Char] = line.toBuffer
	var cur_pos = if (buffer.isEmpty) -1 else 0

	def fixCursorPos = {
		if(cur_pos >= buffer.length)
			cur_pos = buffer.length - 1
		else if(cur_pos < 0)
			cur_pos = 0
	}

	def x = {
		if(cur_pos >= 0 && !buffer.isEmpty) {
			buffer.remove(cur_pos)
			fixCursorPos
		}
	}

	def dw = {
		do
			buffer.remove(cur_pos)
		while(cur_pos < buffer.length && buffer(cur_pos) != ' ')
		fixCursorPos
	}

	def insert(c: Char) = {
		cur_pos += 1
		if(cur_pos >= buffer.length)
			buffer.append(c)
		else
			buffer.insert(cur_pos, c)
		fixCursorPos
	}

	def i(c: Char) = insert(c)

	def iw(word: String) = {
		word.foreach(insert(_))
		insert(' ')
	}

	def l(n: Int = 1) = {
		cur_pos += n
		fixCursorPos
	}

	def h(n: Int = 1) = {
		cur_pos -= n
		fixCursorPos
	}

	override def toString = {
		buffer.mkString("")
	}
}
