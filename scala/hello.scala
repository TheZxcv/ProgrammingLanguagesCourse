object Hello {
	def main(args: Array[String]) {
		println("Hello, world!")
	}
}

object HelloApp extends App {
	println("Hello, world!")
}
