import scala.util.parsing.combinator._
import scala.io.Source

abstract class Term
class Operand() extends Term
case class Number(n: Int) extends Operand
case class Variable(v: String) extends Operand

case class Addition(left: Term, right: Term) extends Term
case class Subtraction(left: Term, right: Term) extends Term
case class Multiplication(left: Term, right: Term) extends Term
case class Division(left: Term, right: Term) extends Term
case class Power(left: Term, right: Term) extends Term
case class Sqrt(x: Term) extends Term
case class Sine(x: Term) extends Term
case class Cosine(x: Term) extends Term
case class Tan(x: Term) extends Term
case class Log(x: Term) extends Term

case class Equal(x: Term, y: Term) extends Term

class ExpressionParserCombinators extends JavaTokenParsers {
	def number = decimalNumber ^^ { case n => Number(n.toInt) }
	def variable = "[a-zA-Z]".r ^^ { case s => Variable(s) }

	def expression: Parser[Term] = (equation) | (term ~ rep(operator_low ~ term)) ^^ {
			case eq ~ List() => { eq }
			case l ~ list =>
				list.foldLeft(l) {
					case (left, "+" ~ right) => Addition(left, right)
					case (left, "-" ~ right) => Subtraction(left, right)
				}
	}
	def term = factor ~ rep(operator_high ~ factor) ^^ {
		case l ~ list => list.foldLeft(l) {
				case (left, "*" ~ right) => Multiplication(left, right)
				case (left, "/" ~ right) => Division(left, right)
			}
	}

	def function: Parser[Term] = fun ~ (function | number | variable | parenthesizedExpression) ^^ {
			case "sin" ~ expr => Sine(expr)
			case "cos" ~ expr => Cosine(expr)
			case "sqrt" ~ expr => Sqrt(expr)
			case "log" ~ expr => Log(expr)
			case "tan" ~ expr => Tan(expr)
	}
	def fun = "sin" | "cos" | "sqrt" | "log" | "tan"

	def parenthesizedExpression = "(" ~> expression <~ ")" ^^ { case e => e }
	def factor = (function | number | variable | parenthesizedExpression | expression) ~ opt("^" ~> expression) ^^ {
			case op ~ None => op
			case op ~ Some(n) => Power(op, n)
	}
	def sign = "+" | "-"

	def operator_low = "+" | "-"
	def operator_high = "*" | "/"

	def equation = (variable <~ "=") ~ expression ^^ {
			case v ~ expr => Equal(v, expr)
	}
}

object adv_exercise02 {
	def main(args: Array[String]) = {
		val e = new ExpressionParserCombinators
		Source.fromFile("script").getLines.foreach {
			expr => println(expr + " = " + e.parseAll(e.expression, expr))
		}
	}
}

