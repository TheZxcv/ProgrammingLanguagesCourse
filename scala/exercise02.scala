object Exercise02 {
	def squared_numbers(list: List[Any]): List[Any] =
		list match {
			case (num: Int)::tl => num*num :: squared_numbers(tl)
			case (num: Short)::tl => num*num :: squared_numbers(tl)
			case (num: Byte)::tl => num*num :: squared_numbers(tl)
			case (num: Long)::tl => num*num :: squared_numbers(tl)
			case (num: Float)::tl => num*num :: squared_numbers(tl)
			case (num: Double)::tl => num*num :: squared_numbers(tl)
			case (li: List[Any])::tl => squared_numbers(li) :: squared_numbers(tl)
			case (tuple: Product)::tl =>
					println("\"" + tuple + "\", tuples are not supported");
					squared_numbers(tl)
			case _::tl => squared_numbers(tl)
			case _ => Nil
		}

	def intersect(li1: List[Any], li2: List[Any]) = {
		//for (e <- li1 if li2.contains(e)) yield e
		li1.filter(li2.contains(_))
	}

	def symmetric_difference(li1: List[Any], li2: List[Any]) = {
		val commons = intersect(li1, li2);
		for (e <- (li1 ++ li2) if !commons.contains(e)) yield e
	}
}
