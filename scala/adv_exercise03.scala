import scala.util.parsing.combinator._
import scala.io.Source

abstract class Operation
case class Plus() extends Operation
case class Minus() extends Operation

class ArithmeticChecker extends JavaTokenParsers {
	override val whiteSpace = """[ \t]+""".r
	def number = wholeNumber ^^ { _.toInt }
	def operator = "+" | "-"
	def newline = "\n"

	def row = number ~ (operator <~ newline) ^^ {
		case n ~ "+" => (n, Plus())
		case n ~ "-" => (n, Minus())
	}
	def last_row = number <~ "=" ^^ { case e => e }
	def rows = rep(row) ~ (last_row <~ newline) ^^ {
		case list ~ e =>
				val li = list :+ (e, Plus())
				val res = calculate(li)
				val maxlen = max(numLength(res), find_maxlength(li))
				(maxlen, res)
	}
	def separator = rep1("-") <~ newline ^^ { _.length }
	def result = number ^^ { case n => n }

	def operations: Parser[Boolean] = rows ~ separator ~ (result <~ opt(newline)) ^^ {
		case (maxlen, actual_res) ~ seplen ~ res =>
				(actual_res == res) && (max(maxlen, numLength(res)) + 2 == seplen)
	}

	def max(a: Int, b: Int) = if (a > b) a else b
	def calculate(list: List[Tuple2[Int, Operation]]) = {
		var res = 0
		var curr_op: Operation = Plus()
		list.foreach {
			_ match {
				case (n, op) =>
					curr_op match {
						case _: Plus => res += n
						case _: Minus => res -= n
					}
					curr_op = op
			}
		}
		res
	}

	def numLength(num: Int) = num.toString.length

	def find_maxlength(list: List[Tuple2[Int, Operation]]) = {
		list.foldLeft(0) {
			(len: Int, b) =>
				b match {
					case (n, _) if numLength(n) > len => numLength(n)
					case _ => len
				}
		}
	}
}

object adv_exercise03 {
	def main(args: Array[String]) = {
		val input = Source.fromFile("Arithmetic.test").mkString
		println(input)

		val ac = new ArithmeticChecker
		ac.parseAll(ac.operations, input) match {
			case ac.Success(res, _) => println(res)
			case x => println(x)
		}
	}
}
