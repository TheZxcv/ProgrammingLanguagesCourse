import scala.util.parsing.combinator._
import scala.io.Source

class CSVParser extends JavaTokenParsers {
	override val skipWhitespace = false
	override val whiteSpace = """[ \t]""".r

	def title = whiteSpace.* ~> "\"" ~> """[^\n\r\"]*""".r <~ "\"" <~ whiteSpace.* ^^ { _.toString }
	def string = """[^,\n\r\"]+""".r ^^ { _.trim }
	def empty = whiteSpace.+ ^^ { case _ => "" }

	def field = title | string | empty ^^ { case str => str }

	def header = row
	def row = rep1sep(field, ",") <~ """\r""".? <~ "\n" ^^ { case list => list }

	def doc = header ~ rep1(row) ^^ {
		case head ~ list => head :: list
	}
}

object CSVFormatter {
	def format(list: List[List[String]]) : String = {
		val li = list.map(_.map(_.length)).transpose.map(_.max)

		val res = list.map {
				(li, _).zipped.map {
					(length, str) =>
						val emptySpace = length - str.length
						" " + str + (" "*emptySpace) + " "
					}.mkString("|", "|", "|")
		}

		val rowlen = li.map(_ + 2).sum + li.length + 1
		val sep = "-"*rowlen + "\n"
		res match {
			case Nil => ""
			case h::tl => sep + h + "\n" + sep + tl.mkString(sep, "\n", "\n"+sep)
		}
	}
}

object CSVParserCLI {
	def main(args: Array[String]) {
		val p = new CSVParser
		args.foreach {
			filename =>
				val src = Source.fromFile(filename)
				val s = src.mkString
				src.close

				p.parseAll(p.doc, s) match {
					case p.Success(res, _) => println(CSVFormatter.format(res))
					case p.Error(msg, _) => println("Error: " + msg)
					case p.Failure(msg, _) => println("Error: " + msg)
				}
		}
	}
}
