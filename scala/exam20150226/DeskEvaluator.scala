import scala.util.parsing.combinator._
import scala.collection.mutable.Map
import scala.io.Source

class DeskCalculator(val table: Map[Char, Int]) extends JavaTokenParsers {
	def program: Parser[Tuple2[Int, Map[Char, Int]]] = ("print" ~> expr) ~ ("where" ~> init) ^^ {
		case f ~ tab => (f(), tab)
	}

	def expr = operand ~ rep("+" ~> operand) ^^ {
		case f ~ list => list.foldLeft(f) { (f, g) => () => f() + g() }
	}

	def operand = opt("+" | "-") ~ (variable | number) ^^ {
		case None ~ f => () => f()
		case Some("+") ~ f => () => f()
		case Some("-") ~ f => () => -f()
	}

	def variable = "[a-zA-Z]".r ^^ { c => () => table.get(c.charAt(0)).get }
	def number = wholeNumber ^^ { n => () => n.toInt }

	def init = repsep(equation, ",") ^^ {
		case _ => table
	}
	def equation = ("[a-zA-Z]".r <~ "=") ~ number ^^ { case v~n => table += (v.charAt(0) -> n()) }
}

object DeskEvaluator {
	def main(args: Array[String]) {
		if (args.length > 0) {
			val table: Map[Char, Int] = Map()
			val d = new DeskCalculator(table)
			Source.fromFile("test.desk").getLines.foreach {
				line =>
					table.clear
					d.parseAll(d.program, line) match {
						case d.Success((res, tab), _) => println(res); println(tab)
						case x => println(x.toString)
					}
			}
		}
	}
}
