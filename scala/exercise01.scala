object Exercise01 {
	def is_palindrome(string: String) = {
		val str = string.toLowerCase().filter(_.isLetter);
		(for (i <- 0 until str.length()/2)
				yield str.charAt(i) == str.reverse(i)
		).fold(true) { (a, b) => a && b }
	}

	def is_an_anagram(string: String, list: List[String]) = {
		val clean_and_sort = (str: String) => str.toLowerCase().filter(_.isLetter).toList.sorted;
		val str = clean_and_sort(string);
		list.map(clean_and_sort(_) == str).fold(false) {(x, y) => x || y}
	}

	def is_prime(n: Int) = (for (i <- 2 until n) yield n % i != 0).fold(true) {(a, b) => a && b}

	def factors(n: Int) = (for (i <- 2 until n if n%i == 0 && is_prime(i)) yield i).toList

	def is_proper(n: Int) = (for (i <- 1 until n if n%i == 0) yield i).fold(0) { (a, b) => a + b} == n
}
